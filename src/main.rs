use clap::{Args, Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap(name = "bip39", version)]
#[command(author, version, about, long_about = None)]
pub struct App {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    /// generate a new random mnemonic
    Generate(GenerateArgs),
}

#[derive(Debug, Args)]
struct GenerateArgs {
    /// language pool to use
    #[clap(value_enum)]
    #[arg(short, long, default_value_t = Language::English)]
    language: Language,
    /// number of words in the generated mnemonic
    #[arg(short, long, default_value_t = 24)]
    n_words: usize,
}

#[derive(Debug, Clone, clap::ValueEnum)]
enum Language {
    English,
    SimplifiedChinese,
    TraditionalChinese,
    Czech,
    French,
    Italian,
    Japanese,
    Korean,
    Spanish,
}

impl Into<bip39::Language> for Language {
    fn into(self) -> bip39::Language {
        match self {
            Self::English => bip39::Language::English,
            Self::SimplifiedChinese => bip39::Language::SimplifiedChinese,
            Self::TraditionalChinese => bip39::Language::TraditionalChinese,
            Self::Czech => bip39::Language::Czech,
            Self::French => bip39::Language::French,
            Self::Italian => bip39::Language::Italian,
            Self::Japanese => bip39::Language::Japanese,
            Self::Korean => bip39::Language::Korean,
            Self::Spanish => bip39::Language::Spanish,
        }
    }
}

fn main() {
    let app = App::parse();
    match app.command {
        Command::Generate(args) => {
            let mnemonic = bip39::Mnemonic::generate_in(args.language.into(), args.n_words)
                .map_err(|e| {
                    eprintln!("failed to generate mnemonic: {e}");
                })
                .unwrap();
            println!("{mnemonic}");
        }
    }
}
