A simple [BIP39](https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki) mnemonic generator using [clap](https://docs.rs/clap/latest/clap/) and [bip39](https://docs.rs/bip39/2.0.0/bip39/).

This supports both choosing your preferred language and length of mnemonic.

```
generate a new random mnemonic

Usage: bip39-clap generate [OPTIONS]

Options:
  -l, --language <LANGUAGE>  language pool to use [default: english] [possible values: english, simplified-chinese, traditional-chinese, czech, french, italian, japanese, korean, spanish]
  -n, --n-words <N_WORDS>    number of words in the generated mnemonic [default: 24]
  -h, --help                 Print help
```

### Installation

```sh
cargo install --git https://gitlab.com/erichulburd/bip39-cli
```
